# -*- coding: utf-8 -*-
"""
Provides a range of plotting classes, used to prepare backtesting results data 
into a various forms/representations suitable for plotting with the Plotly 
package. 

"""
from tsbp.composers import StrategyDataComposer
from tsbp.strategies import RSI, SMAC, SMAO

# colors config for plots
COLORLIST = ['dimgrey', 'darkgrey']
INCLUDE_STATS = [RSI, SMAC, SMAO]


class StrategyPlot:
    """
    The `StrategyPlot` takes pre-prepared data from the `StrategyDataComposer`, 
    and further manipulates this data into a form ready for plotting with 
    Plotly. 
    
    Specifically, the strategy plot depicts winning and losing trades 
    based on a specific strategy, along a timeseries of closing price data for 
    a single company. 
    
    This class constructs two data attributes upon 
    initialisation - one containing the actual data to be plotted, and the other 
    containing the plot's layout parameters. These two datasets can then be 
    assigned to the 'data' and 'layout' attributes of a Plotly figure, 
    respectively.
    
    Args:
        strategy (Strategy instance): the specific strategy instance whose data
            is used to construct a strategy plot.
        company (str): the company whose data is to be plotted.
        start (str): the start dat of the plot, in format 'YYYY-MM-DD'.
        end (str): the end date of the plot, in format 'YYYY-MM-DD'.
        
    """
    def __init__(self, strategy, company, start, end):
        # create a `StrategyDataComposer`, containing strategy data
        composer = StrategyDataComposer(strategy, company, start, end)
        self.data = composer.get_data()
        # unpack this data
        self.index = self.data['close'].index
        self.trades = self.data['trades']
        # assign green or red to individual trades, depending on whether they 
        # incurred a gain or a loss
        self.trades['color'] = (
            self.trades['strategy_return'] >= 0.
        ).map({False: 'tomato', True: 'mediumseagreen'})
        # split the gain and loss trades into separate dataframes
        self.gains = self.trades.where(
            self.trades['strategy_return'] >= 0.
        ).dropna(how='any')
        self.losses = self.trades.where(
            self.trades['strategy_return'] < 0.
        ).dropna(how='any')
        # construct the plot's title
        self.title = (
            '{}: {} to {}<br>Strategy: {}%; market: {}%<br>No. trades: {}'
        )
        # construct plot data and layout, ready for ingestion into plotly
        self.plot_data, self.plot_layout = self.make_plot_data()
        # plot strategy's stats if specified
        if type(strategy) in INCLUDE_STATS:
            self.make_stats_data()
        
    def make_plot_data(self):
        """
        This method constructs the plot data and plot layout data, ready for
        use by Plotly.
        """
        # prepare the close price data (this is presented as a line plot)
        data = [{
            'x': self.index,
            'y': self.data['close'],
            'mode': 'lines',
            'line': {'color': 'navy', 'width': 1.8},
            'name': 'close',
            'text': [
                f"Close: ${close}<br>Date: {str(date)[:10]}" 
                for close, date in zip(self.data['close'], self.index)
            ],
            'hoverinfo': 'text',
        }]            
        # prepare dashed line plots connecting corresponding trade entry and
        # exits
        for i in range(len(self.trades)):
            row = self.trades.iloc[i]
            data.append({
                'x': [row['buy_on'], row['sell_on']],
                'y': [row['buy_at'], row['sell_at']],
                'mode': 'lines',
                'line': {'color': row['color'], 'dash': 'dot', 'width': 1.8},
                'showlegend': False,
                'hoverinfo': 'none'
            })        
        # prepare trade entry points as large grey markers
        data.append({
            'x': self.trades['buy_on'],
            'y': self.trades['buy_at'],
            'mode': 'markers',
            'marker': {'size': 10, 'color': '#ABABAB'},
            'name': 'entries',
            'text': [f"Buy on: {str(date)[:10]}" for date in self.trades['buy_on']],
            'hoverinfo': 'text'
        })        
        # prepare profitable trade exit points as large green markers (colour
        # assigned earlier)
        data.append({
            'x': self.gains['sell_on'],
            'y': self.gains['sell_at'],
            'mode': 'markers',
            'marker': {'size': 10, 'color': self.gains['color'],},
            'name': 'exits (gain)',
            'text': [
                f"Return: {pc}%<br>Sell on: {str(date)[:10]}" 
                for pc, date 
                in zip(self.gains['strategy_return'], self.gains['sell_on'])
            ],
            'hoverinfo': 'text',
        })     
        # prepare non-profitable trade exit points as large red markers (colour
        # assigned earlier)
        data.append({
            'x': self.losses['sell_on'],
            'y': self.losses['sell_at'],
            'mode': 'markers',
            'marker': {'size': 10,'color': self.losses['color'],},
            'name': 'exits (loss)',
            'text': [
                f"Return: {pc}%<br>Exit date: {str(date)[:10]}"
                for pc, date 
                in zip(self.losses['strategy_return'], self.losses['sell_on'])
            ],
            'hoverinfo': 'text',
        })        
        # prepare the plot's layout
        layout = {
            'title': self.title.format(
                self.data['company'], 
                self.data['start'], 
                self.data['end'], 
                self.data['strategy_return'],
                self.data['market_return'],
                len(self.trades)
            ),
            'yaxis': {'title': 'Price ($)', 'showgrid': False, 'zeroline': False},
            'xaxis': {'title': 'Date', 'showgrid': False, 'zeroline': False},
            'hoverdistance': 1,
        }        
        return data, layout
    
    def make_stats_data(self):
        """
        This method pulls out any stats computed by the specific strategy (e.g.
        simple moving averages), and prepares this data for plotting also.
        
        """
        for i, stat in enumerate(self.data['stats']):
            self.plot_data.insert(0, {
                'x': self.index,
                'y': self.data['stats'][stat],
                'mode': 'lines',
                'line': {'width': 1, 'color': COLORLIST[i]},
                'opacity': 0.5,
                'name': stat.replace('_', '-'),
                'hoverinfo': 'none'
            })
            