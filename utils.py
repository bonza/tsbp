import random
import numpy as np
import pandas as pd


def daily_from_annual_mean(annual_mean):
    """
    Determines the required daily mean growth to achieve, on average, an annual
    mean growth specified by `annual_mean`. Given in percent.
    
    Args:
        annual_mean (float): annual growth in percent.
        
    Returns:
        daily_mean (float): required mean daily growth in percent.
    
    """
    annual_mean = (100 + annual_mean) / 100
    daily_mean = 100 * ((annual_mean ** (1 / 261)) - 1)
    return daily_mean


def generate_random_stock(code, start, end, mean, std):
    """
    Generates a random stock price timeseries for a specified length and 
    statistical parameters. The timeseries is constructed incrementally by 
    applying a daily growth factor, drawn from a fixed distribution 
    characterised by `mean` and `std`, to the previous day's price. Returns a 
    dataframe with separate but identical 'open' and 'close' columns - this is 
    done in order for the dataframe to be compatible with the `backtest.py` 
    and `strategy.py` modules.
    
    Args:
        code (str): the name of the randomly generated stock.
        start (str): the start date of the randomly generated data, format
            'YYYY-MM-DD'.
        end (str): the end date of the randomly generated data, format
            'YYYY-MM-DD'.
        mean (float): the mean of the daily growth distribution from which the
            dataset is generated.
        std (float): the std of the daily growth distribution from which the
            dataset is generated.
            
    Returns:
        data (pandas.DataFrame): the randomly generated dataset. Indexed
            by date. Columns are comprised of a two-level pandas.MultiIndex, 
            where the top level (0) is `code`, and bottom level (1) are 'open' 
            and 'close' fields, where the price timeseries for these two fields 
            are the same.
        
    """
    dates = pd.date_range(start, end, freq='B').strftime('%Y-%m-%d')
    price = [1.]
    for i in dates[1:]:
        previous = price[-1]
        price.append(max(0, (1 + random.gauss(mean, std)) * previous))
    data = pd.DataFrame(
        data=np.array([price, price]).T, 
        columns=pd.MultiIndex.from_tuples(
            [(code, 'open'), (code, 'close')], names=['code', 'field']
        ),
        index=dates
    )
    data.index.name = 'date'
    return data
 
    
def generate_random_stocks_db(start, end, mean, std, size=1000, prefix='RAND'):
    """
    Generates a database (pandas.DataFrame) of randomly generated stock 
    timeseries, for a specified length, number of stocks, and statistical
    parameters. Timeseries are constructed incrementally by applying a daily
    growth factor, drawn from a fixed distribution characterised by `mean` and 
    `std`, to the previous day's price.
    
    Args:
        start (str): the start date of the randomly generated data, format
            'YYYY-MM-DD'.
        end (str): the end date of the randomly generated data, format
            'YYYY-MM-DD'.
        mean (float): the mean of the daily growth distribution from which the
            dataset is generated.
        std (float): the std of the daily growth distribution from which the
            dataset is generated.
        size (int, optional): the number of individual stock datasets to
            be generated.
        prefix (str, optional): the prefix for each stock code generated. Each 
            code will be suffixed with a unique integer.
            
    Returns:
        data (pandas.DataFrame): the randomly generated dataset. Indexed
            by date. Columns are comprised of a two-level pandas.MultiIndex, 
            where the top level (0) is the code for each randomly generated 
            stock, and bottom level (1) are 'open' and 'close' fields, 
            where the price timeseries for these two fields are the same.
        
    """
    results = []
    for i in range(size):
        code = f'{prefix}{i}'
        data = generate_random_stock(code, start, end, mean, std)
        results.append(data)
    results = pd.concat(results, axis=1)
    return results
    