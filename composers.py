# -*- coding: utf-8 -*-
"""
The data composers in this module are used to extract the all relevant data 
from a given strategy, needed to create a particular type of plot. This data 
will then be used by the relevant plotting class in `plots` to further prepare 
the data ready for plotting.

"""
import copy
import pandas as pd


class DataComposer:
    """
    The base DataComposer class. Initialises an empty dictionary, to be filled
    by its child class.
    """
    def __init__(self):
        self.data = {}
        
    def get_data(self):
        return self.data
    
    
class AnalystDataComposer(DataComposer):
    """
    Creates a dataset containing strategy and corresponding market 
    (buy-and-hold) performance, suitable for use in significance testing of 
    the strategy's relative performance. In order to generate enough samples,
    it splits the data up into subsets of a specified length (e.g. 6 months), 
    and backtests each of these subsets separately.
    
    Args: 
        strategy (Strategy instance): an instance of a specific strategy class, 
            e.g. `Preconditions` or `SMAC`.
        companies (list of str, or None): the companies of interest. If None,
            then all companies contained within `strategy` data are used.
        start (str): the start date of the backtest data for plotting, format
            'YYYY-MM-DD'.
        end (str): the end date of the backtest data for plotting, format
            'YYYY-MM-DD'.
        sample_int (int): the number of months of data a single backtest 
            sample from the analysis spans.
        
    """
    def __init__(self, strategy, companies, start, end, sample_int):
        DataComposer.__init__(self)
        # make a copy of the strategy, as don't want to modify original 
        # strategy's contents
        strategy = copy.copy(strategy)
        # isolate data from strategy instance for companies and dates of interest
        data = strategy.data
        if not companies:
            companies = list(data.columns.levels[0])
        self.companies = companies
        data = data[self.companies].loc[slice(start, end)]
        # reassign trimmed data to the strategy object, and save strategy 
        # object as  class attribute
        strategy.data = data
        self.strategy = strategy
        # explicitly set start and end dates in case these were None previously
        index = data.index
        self.start = index[0].strftime(format='%Y-%m-%d')
        self.end = index[-1].strftime(format='%Y-%m-%d')
        # construct a set of evenly spaced date intervals according to start 
        # and end dates and `sample_int`, and convert back to string format
        self.intervals = pd.date_range(
            start=self.start, end=self.end, freq=f'{sample_int}MS'
        )
        self.intervals = [
            date.strftime(format='%Y-%m-%d') for date in self.intervals
        ]
        if len(self.intervals) < 2:
            raise ValueError()
        # create the data composer's dataset
        self.compose_data()
        
    def compose_data(self):
        """
        Runs the strategy's `aggregate_backtest` for each date interval 
        specified in `self.intervals`, and assigns the results to `self.data`.
        """
        results = []
        # loop through each consecutive pair of dates in `self.intervals`
        for i in range(len(self.intervals) - 1):
            start = self.intervals[i]
            end = self.intervals[i+1]
            # run an aggregate backtest on the strategy's data using these 
            # dates
            result = (
                self.strategy.aggregate_backtest(
                    self.companies, start=start, end=end, compare=True
                )
            )
            results.append(result)
        # pack results into a dataframe
        results = pd.concat(results)
        # some final tidying of the results dataframe (gets rid of dates)
        results.sort_values(by='code', inplace=True)
        results.reset_index(drop=True, inplace=True)
        # assign the market returns, strategy returns, and the difference
        # between these, to `self.data` as individual datasets.
        self.data['market_return'] = results['market_return']
        self.data['strategy_return'] = results['strategy_return']
        self.data['difference'] = (
            results['strategy_return'] - results['market_return']
        )


class StrategyDataComposer(DataComposer):
    """
    The `StrategyDataComposer` assembles and reformats relevant backtest data 
    from a specific strategy instance and for a single company into a form 
    suitable for ingestion by the `StrategyPlot` class.
    
    Args:
        strategy (Strategy instance): an instance of a specific strategy class, 
            e.g. `Preconditions` or `SMAC`.
        company (str): the company of interest.
        start (str): the start date of the backtest data for plotting, format
            'YYYY-MM-DD'.
        end (str): the end date of the backtest data for plotting, format
            'YYYY-MM-DD'.
            
    """
    def __init__(self, strategy, company, start, end):
        DataComposer.__init__(self)
        # isolate data from the strategy instance for company of interest
        data = strategy.data[company].loc[slice(start, end)]
        self.data['close'] = data['close']
        self.data['company'] = company
        self.data['start'] = start
        self.data['end'] = end
        # run a simple backtest and add results to `self.data`. This is used to
        # plot trade entry and exit points on the plot
        results = strategy.simple_backtest([company], start=start, end=end)
        self.data['trades'] = results
        # run an aggregate backtest and add results to `self.data`. This data
        # is used in the plot's title
        agg_return = strategy.aggregate_backtest(
            [company], start=start, end=end, compare=True
        )
        self.data['strategy_return'] = agg_return.loc[0, 'strategy_return']
        self.data['market_return'] = agg_return.loc[0, 'market_return']
        # add strategy-specific stats to `self.data`
        stats = {}
        for stat in strategy.stats:
            stats[stat] = data[stat]
        self.data['stats'] = stats
        
        
