# -*- coding: utf-8 -*-
"""
Provides a framework for running backtests on a set of data for a given
trading strategy (specific strategies are to be implemented in the separate 
`strategies.py` module).

"""
import numpy as np
import pandas as pd
from scipy.stats import t

  
class Backtester:
    """
    The `Backtester` class provides a framework for running backtests on a set 
    of data for a specific strategy. The `Backtester` class is inherited by
    the `Strategy` class in the `strategies` module, which in turn is inherited
    by a specific strategy class, as declared in that same module.
    
    Args:
        data (pandas.DataFrame): the data over which the backtest is to be run.
    """
    def __init__(self, data):
        # make sure data is sorted by date
        self.data = data.sort_index(axis=1)
        # construct a price lookup table, containing open and close prices
        # (these are the only fields a strategy is allowed to trade on)
        lookup = self.data.loc[slice(None), (slice(None), ['open', 'close'])]
        # shift open prices back one trading day; couldn't find a cleaner way 
        # to do this when dealing with multiindex columns...
        # reshape dataframe to have single-level columns
        lookup = lookup.stack(level=0)
        # groupby company name, and shift open prices back one day
        lookup['open'] = lookup.groupby(axis=0, level=1)['open'].shift(-1)
        # reshape result back to *almost* original shape
        lookup = lookup.unstack(level=1).reorder_levels([1, 0], axis=1)
        # lexsort columns for final result
        self.lookup = lookup.sort_index(axis=1)
        
    def _find_trade_dates(self):
        """
        Placeholder function. An functional `_find_trade_dates` function will 
        be assigned to this class by its grandchild class.
        """
        raise NotImplementedError
    
    @staticmethod    
    def _clean_trade_dates(buy_dates, sell_dates, rel='one_to_one'):
        """
        Makes identified buy and sell dates logically consistent with each
        other, for example, ensures no sell dates occur before all identified
        buy dates, and vice versa. Also ensures that there aren't multiple 
        sell dates for a sinlge buy date, and vice versa.
        
        Args:
            buy_dates (pandas.DatetimeIndex): identified trade entry dates.
            sell_dates (pandas.DatetimeIndex): identified trade exit dates.
            rel (str, default 'one_to_one'): describes the relationship between
                identified buy and sell dates, which depends on the specific 
                strategy. If a strategy necessarily produces one buy date for
                every sell date (e.g. a crossover strategy), then this is 
                'one_to_one' (default). Options are: 'one_to_one', 
                'one_to_many', 'many_to_one', and 'many_to_many'.
            
        Returns:
            buy_dates (pandas.DatetimeIndex): trade entry dates, now logically 
                consistent with `sell_dates`.
            sell_dates (pandas.DatetimeIndex): trade exit dates, now logically
                consistent with `buy_dates`.
                
        Raises:
            ValueError if no logically consistent buy or sell dates exist.
        """
        # if `buy_dates` or `sell_dates` are empty, then there are no trades
        # to evaluate
        if (len(buy_dates) == 0) or (len(sell_dates) == 0):
            raise ValueError('Empty dates were supplied.')            
        # drop all buy dates that occur after the final sell date as these 
        # instances must be incomplete trades
        buy_dates = buy_dates.where(buy_dates < sell_dates[-1]).dropna()
        # if after this `buy_dates` is empty, then there are no trades to
        # evaluate
        if (len(buy_dates) == 0):
            raise ValueError('No complete trades pairs exist.')
        # drop all sell dates that occur before the first buy date as these 
        # instances must be incomplete trades    
        sell_dates = sell_dates.where(sell_dates > buy_dates[0]).dropna()
        # if after this `sell_dates` is empty, then there are no trades to
        # evaluate
        if (len(sell_dates) == 0):
            raise ValueError('No complete trades pairs exist!')
            
        # if the strategy necessarily produces one-to-one buy and sell pairs, 
        # then no more processing of the dates is needed
        if rel == 'one_to_one':
            return buy_dates, sell_dates
        # otherwise identify alternating single buy and sell dates
        else:
            # if strategy produces buy and sell dates with one to many or many 
            # to many relationships, then trim the sell dates such that only
            # the first sell date after each buy date is kept
            if rel in ('one_to_many', 'many_to_many'):
                updated_sell_dates = [
                    sell_dates.where(sell_dates > buy_date).dropna()[0]
                    for buy_date in buy_dates
                ]
                sell_dates = (
                    pd.DatetimeIndex(updated_sell_dates).drop_duplicates()
                )
            # if strategy produces buy and sell dates with a many to one or 
            # many to many relationship, then trim the buy dates such that only
            # the first buy date between every two consecutive already trimmed
            # sell dates is kept
            if rel in ('many_to_one', 'many_to_many'):
                updated_buy_dates = [buy_dates[0]]
                for i in range(len(sell_dates) - 1):   
                    updated_buy_dates.append(
                        buy_dates.where(
                            (buy_dates > sell_dates[i]) & 
                            (buy_dates < sell_dates[i+1])
                        ).dropna()[0]
                    )
                buy_dates = pd.DatetimeIndex(updated_buy_dates)
            # now the date lists are logical with respect to each other      
            return buy_dates, sell_dates
            
    def _get_trade_prices(self, company, buy_dates, buy_on, sell_dates, sell_on):
        """
        Tabulates all trade entry and exit prices according to the provided
        buy and sell dates.
        
        Args:
            company (str):
            buy_dates (pandas.DatetimeIndex):
            sell_dates (pandas.DatetimeIndex):
        
        Returns:
            buy_prices (np.array):
            sell_prices (np.array):
        """
        buy_prices = self.lookup.loc[buy_dates, (company, buy_on)].values
        sell_prices = self.lookup.loc[sell_dates, (company, sell_on)].values
        return buy_prices, sell_prices
    
    @staticmethod
    def _get_trade_returns(buy_prices, sell_prices, fee):
        """
        Calculates the return in percent per trade. Subtracts a specified 
        trading fee from each result.
        
        Args:
            buy_prices (np.array): the entry prices for each trade.
            sell_prices (np.array): the exit prices for wach trade.
            fee (float): nominal trading fee, in percent.
            
        Returns:
            returns (np.array): the return in percent of each trade.
            
        """
        # adjust the buy and sell prices to account for trading fees
        adj_buy_prices = buy_prices + (fee / 100. * buy_prices)
        adj_sell_prices = sell_prices - (fee / 100. * sell_prices)
        # calculate the returns as percentages of the adjusted entry prices
        returns = 100 * (adj_sell_prices - adj_buy_prices) / adj_buy_prices
        returns = np.round(returns, 2)
        return returns
    
    def _get_market_returns(self, companies=None, start=None, end=None, fee=0.0):
        """
        Calculates the percent change in closing price for each company over a 
        specified period.
        
        Args:
            companies (list, default None): list of companies in `self.lookup` 
                to calculate price changes for.
            start (str, default None): start date of price change data. If None
                (default), then the earliest data in `self.lookup` is used. 
                Format 'YYYY-MM-DD'.
            end (default None): end date of price change data. If None (default), 
                then the latest data in `self.lookup` is used. Format 
                'YYYY-MM-DD'.
            fee (float): nominal trading fee, in percent.
        
        Returns:
            returns (pandas.DataFrame): contains the change in price in percent
                between `start` and `end` dates for each company in 
                `companies`.
        """
        # trim a copy of `self.lookup` to date range denoted by `start` and 
        # `end`, for the specified companies
        returns = self.lookup.loc[
            slice(start, end), 
            (slice(None) if not companies else companies, 'close')
        ].copy()
        # save initial (start date) and final (end date) prices to variables,
        # adjust to incorporate trading fees
        initial = returns.iloc[0]
        initial += (fee / 100. * initial)
        final = returns.iloc[-1]
        final -= (fee / 100. * final)
        # calculate the price change in percent between start and end dates
        returns = 100 * (final - initial) / initial
        # do some pretty formatting of the resulting dataframe
        returns = returns.reset_index(level=0).reset_index(drop=True)
        returns.columns = ['code', 'market_return']
        return returns
    
    @staticmethod
    def _random_sequence(data):
        """
        Generates a randomly selected sequence of non-overlapping consecutive
        trades from `data`. For use with `self.chronological_backtest`.
        
        Args:
            data (pandas.DataFrame): contains individual trades, as produced by
                `self.simple_backtest`.
                
        Returns:
            result (pandas.DataFrame): a randomly selected sequence of 
                non-overlapping consecutive trades.
        """
        result = pd.DataFrame()
        # while the data frame hasn't been exhausted:
        while not data.empty:
            # determine the earliest possible buy date in the data
            buy_on = data['buy_on'].min()
            # create a new dataframe containing all trades occuring on that 
            # earliest date
            samples = data[data['buy_on'] == buy_on]
            # choose a trade at random (so as to not favour the first in the 
            # list for example), append to results
            trade = samples.sample()
            result = pd.concat([result, trade])
            # get the chosen trade's sell date
            sell_on = trade.iloc[0]['sell_on']
            # trim `data` to only contain buy dates after `sell_on`, repeat
            data = data[data['buy_on'] > sell_on]
        result = result.reset_index(drop=True)
        return result
               
    def simple_backtest(
            self, companies, start=None, end=None, fee=0.0, buy_on='close', 
            sell_on='close', **kwargs):
        """
        Identifies all trades for a set of companies over a specified date
        range, and calculates the individual returns in percent for each trade.
        
        Args:
            companies (list): companies to be included in the backtest.
            start (str, default None): start date of the backtest. If None 
                (default), earliest date in `self.data` is used. Format 
                'YYYY-MM-DD'.
            end (str, default None): end date of the backtest. If None (default), 
                latest date in `self.data` is used. Format 'YYYY-MM-DD'.
            fee (float): nominal trading fee, in percent.
            buy_on (str, default 'close'): the field from which trade entries
                are determined.
            sell_on (str, default 'close'): the field from which trade exits are
                determined.
            
        Returns:
            results (pandas.DataFrame): contains buy and sell dates and prices,
                and trade return in percent, for each individual trade
                identified in `self.data`.
            
        """
        results = []
        columns = [
            'code', 'buy_on', 'buy_at', 'sell_on', 'sell_at', 'strategy_return'
        ]
        for company in companies:
            # try to identify trade dates for each company
            try:
                buy_dates, sell_dates = self._find_trade_dates(
                    company, start=start, end=end, **kwargs
                )
                # get trade prices for identified trade dates
                buy_prices, sell_prices = self._get_trade_prices(
                    company, buy_dates, buy_on, sell_dates, sell_on
                )
                # calculate returns for each identified trade
                returns = self._get_trade_returns(
                    buy_prices, sell_prices, fee=fee
                ) 
                # covert all of this data to a dataframe, and append to `results`
                data = [
                    [company] * len(returns), buy_dates, buy_prices, 
                    sell_dates, sell_prices, returns
                ]
                results.append(pd.DataFrame(dict(zip(columns, data))))
            # if no trade dates are found, then return an empty dataframe
            except ValueError:
                results.append(pd.DataFrame(columns=columns))
        # concatenate the results into a single dataframe
        results = pd.concat(results, axis=0)
        results = results.reset_index(drop=True)
        results.dropna(inplace=True)
        return results
    
    def aggregate_backtest(
            self, companies, start=None, end=None, fee=0.0, buy_on='close', 
            sell_on='close', compare=False, **kwargs):
        """
        
        Calculates the aggregated strategy return in percent for each company 
        over the given period. Assumes all available capital is invested in 
        each subsequent trade (i.e. compound). Offers the additional option to 
        calculate the hypothetical return for the same period due to a 'buy and 
        hold' strategy.
        
        Args:
            companies (list): companies to be included in the backtest.
            start (str, default None): start date of the backtest. If None 
                (default), earliest date in `self.data` is used. Format 
                'YYYY-MM-DD'.
            end (str, default None): end date of the backtest. If None (default), 
                latest date in `self.data` is used. Format 'YYYY-MM-DD'.
            fee (float): nominal trading fee, in percent.
            buy_on (str, default 'close'): the field from which trade entries
                are determined.
            sell_on (str, default 'close'): the field from which trade exits are
                determined.
            compare (bool, default False): if True, includes an additional
                'market_return' field, whichs represents returns from 'buy and 
                hold' strategy over the same period.
                
        Returns:
            results (pandas.DataFrame): contains aggregated strategy return 
                in percent for each company over the specified period. Also
                contains equivalent 'buy and hold' return if `compare` is True.
                        
        """
        # start with a dataframe containing all individual trades within the
        # period
        results = self.simple_backtest(
            companies, start=start, end=end, fee=fee, buy_on=buy_on, 
            sell_on=sell_on, **kwargs
        )[['code', 'strategy_return']]
        
        # convert percentage returns to fractional returns
        results['strategy_return'] = (100 + results['strategy_return']) / 100
        # group data by company and take the product of fractional returns for
        # each company
        results = results.groupby('code').prod().reset_index()
        # convert these aggregated returns back to percentages
        results['strategy_return'] = 100 * (results['strategy_return'] - 1)
        # include a nan for any company for which no trades were found
        missing_df = pd.DataFrame(columns=['code', 'strategy_return'])
        missing_df['code'] = [
            company for company in companies if company not in list(results['code'])
        ]
        results = pd.concat([results, missing_df], ignore_index=True)
        results = results.sort_values(by='code', ascending=True)
        if compare:
            # get buy and hold returns for each company (incorporates fees)
            market = self._get_market_returns(
                companies, start=start, end=end, fee=fee
            )
            # concatenate the two dataframes
            for df in [results, market]:
                df.set_index('code', drop=True, inplace=True)
            results = pd.concat([results, market], axis=1, sort=True)
            # make results look nicer with an ascending index
            results.reset_index(inplace=True)
        else:
            results.reset_index(inplace=True, drop=True)
        results = np.round(results, 2)
        return results
        
    def chronological_backtest(
            self, companies, start=None, end=None, fee=0.0, buy_on='close', 
            sell_on='close', trials=1000, **kwargs):
        """
        Offers a more realistic backtesting approach, whereby only a single
        trade can be active at once (i.e. no overlapping trade dates). Given
        there are theoretically many trades to choose from at any one time, 
        trade sequences are generated by a random selection of non-overlapping 
        trades. This process can be repeated a specified number of times in 
        order to form a statistical representation of the performance of a
        strategy over a given period.
        
        Args:
            companies (list): companies to be included in the backtest.
            start (str, default None): start date of the backtest. If None 
                (default), earliest date in `self.data` is used. Format 
                'YYYY-MM-DD'.
            end (str, default None): end date of the backtest. If None (default), 
                latest date in `self.data` is used. Format 'YYYY-MM-DD'.
            fee (float): nominal trading fee, in percent.
            buy_on (str, default 'close'): the field from which trade entries
                are determined.
            sell_on (str, default 'close'): the field from which trade exits are
                determined.
            trials (int, default 1000): the number 
            
        Returns:
            result (pandas.DataFrame): contains the strategy return for each
                trial.
        """
        # first run a simple backtest to identify all possible trades within
        # time period
        trades = self.simple_backtest(
            companies, start=start, end=end, fee=fee, buy_on=buy_on, 
            sell_on=sell_on, **kwargs
        )
        columns = ['strategy_return', 'no_trades']
        result = pd.DataFrame(columns=columns)
        for i in range(trials):
            # generate random sequence and calculate overall return
            sequence = self._random_sequence(trades)
            returns = (100 + sequence['strategy_return']) / 100
            returns = np.round(100 * (returns.prod() - 1), 2)
            # also determine number of trades in that sequence
            no_trades = len(sequence)
            # pack this into a dataframe and append to results
            new_df = pd.DataFrame([[returns, no_trades]], columns=columns)
            result = pd.concat([result, new_df])
        result = result.reset_index(drop=True)
        return result

    def summarise(
            self, companies, start=None, end=None, fee=0.0, buy_on='close', 
            sell_on='close', **kwargs):
        """
        """
        #
        results = self.aggregate_backtest(
            companies, start=start, end=end, fee=fee, buy_on=buy_on, 
            sell_on=sell_on, compare=True, **kwargs
        )
        #
        means = np.round(results.mean(), 2)
        stds = np.round(results.std(), 2)
        #
        diff = results['strategy_return'] - results['market_return']
        diff_mean = diff.mean()
        diff_std = diff.std()
        cbt = np.round(
            100 * (1 - t.cdf(0., len(diff)-1, loc=diff_mean, scale=diff_std)), 2
        )
        print(f"Mean market return:               {means['market_return']}%")
        print(f"Standard dev. market returns:     {stds['market_return']}%")
        print(f"Mean strategy return:             {means['strategy_return']}%")
        print(f"Standard dev. strategy returns:   {stds['strategy_return']}%")
        print(f"Likelihood strategy > market:     {cbt}%")
        