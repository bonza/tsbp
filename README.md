# About

The **T**rading **S**trategy **B**acktest **P**ackage, or `tsbp` for short, is a 
package designed to facilitate easy implementation and automated backtesting of 
stock market trading strategies. The package includes some simple in-built 
strategies, but it is also designed such that new stratgies can be easily 
written, implemented and backtested within the package's framework. 

Aside from regular Python packages such as `pandas` and `numpy`, `tsbp` is also
dependent on the `sdmi` package, available at `https://gitlab.com/bonza/sdmi`.

# Usage