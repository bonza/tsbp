# -*- coding: utf-8 -*-
"""
This module implements specific trading strategies as unique class objects.
Strategy classes inherit the `Backtester` class from the `backtest` module to
enable in-built backtesting of their trading strategy.

"""
from tsbp.stats import (
    consecutive_days, relative_change, rsi, sma, DEFAULT_LABELS
)
from tsbp.backtest import Backtester


class Strategy(Backtester):
    """
    Basic strategy class. Inherits the `Backtester` class from the `backtest` 
    module, enabling built-in backtesting of a given strategy against supplied 
    data.
    
    Args:
        data (pandas.DataFrame): contains the data against which a strategy is
            to be backtested.
    """   
    def __init__(self, data):
        Backtester.__init__(self, data)
        self.stats = []


class Preconditions(Strategy):
    """
    The `Preconditions` strategy identifies trade entry dates based on daily 
    growth thresholds over a certain number of prior consecutive days. For 
    example, trade entries might correspond to dates where the previous three 
    consecutive days all saw daily growth of more than 1%. Sell dates are
    identified as a fixed number of days after the corresponding buy dates.
    
    Args:
        data (pandas.DataFrame): the data on which to backtest the strategy.
        no_days (int): the number of consecutive days the daily relative
            growth criterium must be met before a trade entry is identified.
        mode (str): 
        thresh (float): the criterium's daily relative growth threshold.
        wait_to_buy (int, >= 0): the number of days to wait after the entry 
            criterium is met before entering a trade.
        sell_after (int, >= 1): the number of days to wait before exiting a
            trade.
            
    """
    def __init__(
            self, data, no_days, mode, thresh, wait_to_buy=0, sell_after=1):
        # initialise the base `Strategy` class
        Strategy.__init__(self, data)
        self.no_days = no_days
        self.sell_after = sell_after
        self.wait_to_buy = wait_to_buy
        self.name = 'preconditions_{}_days_{}{}'.format(no_days, mode, thresh)
        # calculate daily relative change first, as consecutive days 
        # calculation is based on this
        self.data = relative_change(
            self.data, 'close', -1, 'close', 0, inplace=True, label='rc'
        )
        # calculate consecutive days
        self.data = consecutive_days(
            self.data, 'rc', thresh, mode=mode, inplace=True
        )
        # assign consecutive days label as a strategy attribute, for lookup
        # in `_find_trade_dates` method
        label = DEFAULT_LABELS['consecutive_days'].format('rc', mode, thresh)
        self.stats.append(label)
        setattr(self, f'consec_days_label', label)
        
    def _find_trade_dates(self, company, start=None, end=None):
        """
        Finds all buy and sell trading date pairs using the Preconditions 
        strategy for a single company over a specified period.
        
        Args:
            company (str): the stock to backtest this strategy on.
            start (str, default None): the start date of the backtest, 
                format YYYY-MM-DD.
            end (str, default None): the end date of the backtest, 
                format YYYY-MM-DD.
                
        Returns:
            buy_dates (pandas.DatetimeIndex): the identified trade 
                entry dates.
            sell_dates (pandas.DatetimeIndex): the identified trade 
                exit dates.
                
        """
        # trim `self.data` to the company and dates of interest
        data = self.data[company].loc[slice(start, end)]
        # shift consecutive days data forward by `self.wait_to_buy` days
        label = self.consec_days_label
        data[label] = data[label].shift(periods=self.wait_to_buy)
        index = data.index
        # reset index of `data` to integers
        data = data.reset_index()
        # pull out consecutive days meeting criteria, and remove final date as
        # don't want to identify trade dates outside of available range
        consec_data = data[self.consec_days_label].iloc[:-self.sell_after]
        # get indices for buy dates
        buy_inds = (
            consec_data.where(consec_data == self.no_days).dropna().index
        )
        # sell date occurs `self.sell_after` trading days after buy date
        buy_dates = index[buy_inds]
        sell_dates = index[buy_inds + self.sell_after]
        # ensure buy and sell dates are logically consistent
        buy_dates, sell_dates = self._clean_trade_dates(buy_dates, sell_dates)
        return buy_dates, sell_dates
    
    
class RSI(Strategy):
    """
    This strategy uses the RSI as an indicator of over-sold and over-bought 
    conditions, and enters trades in over-sold conditions, and exits trades in
    over-bought conditions
    
    Args:
        data (pandas.DataFrame): the data on which to backtest the strategy.
        window (int): size of the rolling window in days, used to calculate
            average gains and losses as necessary for the RSI calculation.
        buy_thresh (float): the RSI threshold below which conditions are 
            considered over-sold (and trades are entered).
        sell_thresh (float): the RSI threshold above which conditions are 
            considered over-bought (and trades are exited).
            
    """
    def __init__(self, data, window, buy_thresh=40., sell_thresh=60.):
        Strategy.__init__(self, data)
        self.buy_thresh = buy_thresh
        self.sell_thresh = sell_thresh
        self.name = 'rsi_{}'.format(window)
        # calculate the RSI on `self.data`
        self.data = rsi(self.data, window, inplace=True)
        # construct label for RSI data and assign as an attribute
        rsi_label = DEFAULT_LABELS['rsi']
        label = rsi_label.format(window)
        self.stats.append(label)
        setattr(self, f'rsi_label', label)
        
    def _find_trade_dates(self, company, start=None, end=None):
        """
        Finds all buy and sell trading date pairs using the RSI strategy for a 
        single company over a specified period.
        
        Args:
            company (str): the stock to backtest this strategy on.
            start (str, default None): the start date of the backtest, 
                format YYYY-MM-DD.
            end (str, default None): the end date of the backtest, 
                format YYYY-MM-DD.
                
        Returns:
            buy_dates (pandas.DatetimeIndex): the identified trade 
                entry dates.
            sell_dates (pandas.DatetimeIndex): the identified trade 
                exit dates.
                
        """
        # trim `self.data` to the company and dates of interest
        data = self.data[company].loc[slice(start, end)]
        rsi_data = data[self.rsi_label]
        # potential buy dates are where RSI indicates over-sold conditions
        buy_dates = rsi_data.where(rsi_data < self.buy_thresh).dropna().index
        # potential sell dates are where RSI indicates over-bought conditions 
        sell_dates = rsi_data.where(rsi_data > self.sell_thresh).dropna().index
        # clean buy and sell dates to get logically constistent entry/exit pairs
        buy_dates, sell_dates = self._clean_trade_dates(
            buy_dates, sell_dates, rel='many_to_many'
        )        
        return buy_dates, sell_dates

    
    
class SMAC(Strategy):
    """
    The Simple Moving Average Crossover. This strategy uses the intersection of 
    an SMA and the daily close price to determine trade entry and exit points.
    If a trade entry occurs on a crossover of upward momentum, then the
    corresponding exit occurs on a crossover of downward momentum, and vice
    versa.
    
    Args:
        data (pandas.DataFrame): the data on which to backtest the strategy.
        field (str): the field in data over which the SMA is calculated.
        window (int): the window size for the SMA.
        momentum (str, either 'up' (default) or 'down'): specifies whether to
            buy on upward momentum (i.e. when the SMA crosses the close price)
            or downward momentum.
            
    """
    momentum_dict = {'up': 1, 'down': -1}
    
    def __init__(self, data, field, window, momentum='up'):        
        Strategy.__init__(self, data)
        self.momentum = momentum
        self.name = 'smac_{}'.format(window)            
        # calculate the SMA on `self.data`
        self.data = sma(self.data, field, window, inplace=True)
        # construct label for SMA data and assign as an attribute
        sma_label = DEFAULT_LABELS['sma']
        label = sma_label.format(field, window)
        self.stats.append(label)
        setattr(self, f'sma_label', label)
        
    def _find_trade_dates(self, company, start=None, end=None):
        """
        Finds all buy and sell trading date pairs using the SMAC strategy for a 
        single company over a specified period.
        
        Args:
            company (str): the stock to backtest this strategy on.
            start (str, default None): the start date of the backtest, 
                format YYYY-MM-DD.
            end (str, default None): the end date of the backtest, 
                format YYYY-MM-DD.
                
        Returns:
            buy_dates (pandas.DatetimeIndex): the identified trade 
                entry dates.
            sell_dates (pandas.DatetimeIndex): the identified trade 
                exit dates.
                
        """
        # trim `self.data` to the company and dates of interest
        data = self.data[company].loc[slice(start, end)]
        # isolate close and SMA data
        close_data = data['close']
        sma_data = data[self.sma_label]
        # calculate the difference in close and SMA data. Multiply this by 1 or
        # -1 depending on whether buying on upward or downward momentum
        diff = SMAC.momentum_dict[self.momentum] * (close_data - sma_data)
        diff_shift = diff.shift(1)
        # buy dates occur where `diff` is above zero for the current day, and
        # below zero for the previous day
        buy_dates = data.where((diff_shift < 0.) & (diff >= 0.)).dropna().index
        # sell dates occur where `diff` is below zero for the current day, and
        # above zero for the previous day
        sell_dates = data.where((diff_shift >= 0.) & (diff < 0.)).dropna().index
        # ensure buy and sell dates are logically consistent
        buy_dates, sell_dates = self._clean_trade_dates(buy_dates, sell_dates)        
        return buy_dates, sell_dates
    
    
class SMAO(Strategy):
    """
    Simple Moving Average Oscillator. This strategy uses the crossover points 
    of two different SMAs ('short' and 'long') as trade entry and exit dates.
    
    Args:
        data (pandas.DataFrame): the data on which to backtest the strategy.
        short_field (str): the field in data over which the 'short' SMA is 
            calculated.
        short_window (int): the window size for the 'short' SMA.
        long_field (str): the field in data over which the 'long' SMA is 
            calculated.
        long_window (int): the window size for the 'long' SMA.
            
    """    
    def __init__(
            self, data, short_field, short_window, long_field, long_window):
        # initialise the base `Strategy` class
        Strategy.__init__(self, data)
        self.name = 'smao_{}_{}'.format(short_window, long_window)            
        sma_label = DEFAULT_LABELS['sma']
        windows_dict = {
            'short': (short_field, short_window), 'long': (long_field, long_window)
        }
        # determine 'short' and 'long' SMAs from the data, construct labels
        # for each of these fields
        for key, (field, window) in windows_dict.items():
            self.data = sma(self.data, field, window, inplace=True)
            label = sma_label.format(field, window)
            self.stats.append(label)
            # assign SMA labels as strategy attributes, for lookup in 
            # `_find_trade_dates` method 
            setattr(self, f'{key}_sma_label', label)
                    
    def _find_trade_dates(self, company, start=None, end=None):
        """
        Finds all buy and sell trading date pairs using the SMAO strategy for a 
        single company over a specified period.
        
        Args:
            company (str): the stock to backtest this strategy on.
            start (str, default None): the start date of the backtest, 
                format YYYY-MM-DD.
            end (str, default None): the end date of the backtest, 
                format YYYY-MM-DD.
                
        Returns:
            buy_dates (pandas.DatetimeIndex): the identified trade 
                entry dates.
            sell_dates (pandas.DatetimeIndex): the identified trade 
                exit dates.
                
        """
        # trim `self.data` to the company and dates of interest
        data = self.data[company].loc[slice(start, end)]
        # calculate the difference in the two SMAs
        sma_diff = data[self.short_sma_label] - data[self.long_sma_label]
        sma_diff_shift = sma_diff.shift(1)
        # buy dates occur at any upward crossover of the short SMA (when the
        # difference becomes less than zero)
        buy_dates = sma_diff.where(
            (sma_diff_shift <= 0.) & (sma_diff > 0.)
        ).dropna().index
        # sell dates occur at any downward crossover of the short SMA (when the
        # difference becomes greater than zero)
        sell_dates = sma_diff.where(
            (sma_diff_shift > 0.) & (sma_diff <= 0.)
        ).dropna().index
        # ensure buy and sell dates are logically consistent
        buy_dates, sell_dates = self._clean_trade_dates(buy_dates, sell_dates)         
        return buy_dates, sell_dates
