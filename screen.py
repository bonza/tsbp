# -*- coding: utf-8 -*-
"""
This module provides the tools for analysing data for a set of stocks to 
identify current trading actions under a given trading strategy.

"""
import numpy as np
import pandas as pd
from operator import lt, gt
from sdmi import load
from tsbp import stats


class Screener:
    """
    The `Screener` base class provides initialisation and other methods common 
    to all strategy-specific screening classes.
    
    Args:
        db_path (str): the absolute location of the database.
        stocks (list of str or None): list of stocks to be screened for trade 
            entries and exits. If None, all stocks located within `db_path`
            will be used.
            
    """
    def __init__(self, db_path, stocks):
        loader = load.Loader(db_path)
        self.data = loader.load(stocks=stocks)
        
    def current_offline_data(self):
        """
        Gets the most recent data for all stocks contained within `self.data`;
        this corresponds to the close price on the last available date.
        
        Returns:
            result (pandas.DataFrame): the close price from the most recent 
                available date within `self.data`, for all available stocks
                within `self.data`.
                
        """
        # get most recent offline data (previous close)
        result = self.data.iloc[-1]
        result = result.xs('close', axis=0, level=1).to_frame()
        result.columns = ['Current']
        result = np.round(result, 2)
        return result
    

class SMAOScreener(Screener):
    """
    The `SMAOScreener` uses the SMAO strategy to detect trade entry/exit points
    for a current set of data.
    
    Args:
        db_path (str): the absolute location of the database.
        stocks (list of str or None): list of stocks to be screened for trade 
            entries and exits using the SMAO strategy. If None, all stocks 
            located within `db_path` will be used.
        short_field (str): the field over which the 'short' SMA for the SMAO 
            strategy is calculated.
        short_window (int): the window size for the 'short' SMA.
        long_field (str): the field over which the 'long' SMA for the SMAO 
            strategy is calculated.
        long_window (int): the window size for the 'long' SMA.
        
    """
    def __init__(
            self, db_path, stocks, short_field, short_window, long_field, 
            long_window,
        ):
        Screener.__init__(self, db_path, stocks)
        self.short_field = short_field
        self.short_window = short_window
        self.long_field = long_field
        self.long_window = long_window
        # trim data back to only the necessary number of days to save memory
        self.data = (
            self.data.iloc[-(max(self.short_window, self.long_window)+1):]
        )
        
    def calculate_target(self):
        """
        Determines the current closing price which will result in the short
        and long SMAs being equal. This corresponds to the threshold price for
        trading action for a SMAO strategy.
        
        Returns:
            target (pandas.DataFrame): the target price for each stock within
                `self.data`.
                
        """
        # get the sum of previous prices spanning 'short_window - 1' days
        short_data = self.data.xs(self.short_field, axis=1, level=1)
        short_sum = short_data.rolling(self.short_window-1).sum().iloc[-1]
        # get the sum of previous prices spanning 'long_window - 1' days
        long_data = self.data.xs(self.long_field, axis=1, level=1)
        long_sum = long_data.rolling(self.long_window-1).sum().iloc[-1]
        # calculate the target price for current day (price for which the 
        # current short and long smas will be equal)
        target = (
            (self.short_window * long_sum - short_sum * self.long_window) /
            (self.long_window - self.short_window)
        ).dropna()
        # format target and return
        target = np.round(target.to_frame(name='Target'), 2)
        return target
    
    def initial_action(self):
        """
        Assigns a provisional 'Wait to buy' or 'Wait to sell' action to each 
        stock depending on whether the most recent short SMA value is below or 
        above the long SMA value, respectively. Actual buy or sell situations 
        are subsequently identified by the method `self.get_screener_data`, and 
        where appropriate, these provisional actions are replaced with 'BUY' or
        'SELL'.
        
        Returns:
            action (pandas.DataFrame): contains the provisional action ('Wait
                to buy' or 'Wait to sell') for each stock in `self.data`, based 
                on the relative positions of the short and long SMAs.
                
        """
        # calculate smas for previous day
        prev_short = stats.sma(
            self.data, self.short_field, self.short_window, label='short'
        ).xs('short', axis=1, level=1).iloc[-1]
        prev_long = stats.sma(
            self.data, self.long_field, self.long_window, label='long'
        ).xs('long', axis=1, level=1).iloc[-1]
        # take difference, abandon any nans
        diff = (prev_short - prev_long).dropna()
        # where short sma less than long, this corresponds to 'Waiting to buy', 
        # and vice versa. When cross occurs, this switches to 'BUY'
        action = (diff <= 0.).map({True: 'Wait to buy', False: 'Wait to sell'})
        action = action.to_frame(name='Action')
        return action
    
    def get_screener_data(self):
        """
        Replaces any provisional actions assigned by `self.initial_action` with
        'BUY' or 'SELL' actions, where such situations are identified. Returns
        a summary dataframe containing current price, target price, and
        corresponding action, for each stock in `self.data`.
        
        Returns:
            result (pandas.DataFrame): contains the current price (i.e. most 
                recent close price), the target price, and the current action, 
                for each stock within `self.data`.
                   
        """
        current = self.current_offline_data()
        target = self.calculate_target()
        action = self.initial_action()
        # if the short window is less than the long window, then a sell is
        # triggered when the short sma crosses below the long sma, and vice
        # versa. The following lines define these actions.
        lt_mode, gt_mode = (
            ('sell', 'buy') if self.short_window < self.long_window 
            else ('buy', 'sell')
        )
        lt_action, gt_action = (
            'Wait to {}'.format(lt_mode), 'Wait to {}'.format(gt_mode)
        )
        # put all price data into a dataframe
        result = pd.concat([current, target, action], axis=1, sort=True)
        result.index.name = 'code'
        # now replace 'Wait to buy' with 'BUY' where buy condition is met, and
        # same for sell.
        action_dict = {
            lt_action: {'func': lt, 'replace': f'{lt_mode.upper()}'},
            gt_action: {'func': gt, 'replace': f'{gt_mode.upper()}'},
        }
        for key in action_dict:
            sub_dict = action_dict[key]
            mask = (
                (result['Action'] == key) & 
                sub_dict['func'](result['Current'], result['Target'])
            )
            # upgrade action wherever mask is True
            result['Action'].where(
                ~mask, other=sub_dict['replace'], inplace=True
            )
        result.dropna(inplace=True)
        return result
    
    
class RSIScreener(Screener):
    """
    The `RSIScreener` uses the RSI strategy to detect trade entry/exit points
    for a current set of data.
    
    Args:
        db_path (str): the absolute location of the database.
        stocks (list of str or None): list of stocks to be screened for trade 
            entries and exits using the RSI strategy. If None, all stocks 
            located within `db_path` will be used.
        window (int): size of the rolling window in days, used to calculate
            average gains and losses as necessary for the RSI calculation.
        buy_thresh (float): the RSI threshold below which conditions are 
            considered over-sold (and trades are entered).
        sell_thresh (float): the RSI threshold above which conditions are 
            considered over-bought (and trades are exited).
        
    """
    def __init__(
            self, db_path, stocks, window, buy_thresh=40., sell_thresh=60., 
            
        ):
        Screener.__init__(self, db_path, stocks)
        self.window = window
        self.buy_thresh = buy_thresh
        self.sell_thresh = sell_thresh
        # calculate window-1 up and down sums here as used for both target calcs
        window -= 1
        ups = stats._average_gain_or_loss(self.data, window, '>') * window
        self.ups = ups.xs('gain', axis=1, level=1).iloc[-1]
        downs = stats._average_gain_or_loss(self.data, window, '<') * window
        self.downs = downs.xs('loss', axis=1, level=1).iloc[-1]
        self.prov_rs = self.ups / self.downs
        
    def calculate_target(self, mode):
        """
        Determines the current closing price which will result in one of the 
        lower or upper RSI thresholds (`self.buy_thresh` and `self.sell_thresh`
        respectively) being met.
        
        Args:
            mode ('buy' or 'sell'): indicates whether the lower ('buy') or 
                upper ('sell') threshold/target is being sought.
        
        Returns:
            target (pandas.DataFrame): the target price for each stock within
                `self.data`.
                
        """
        if mode == 'buy':
            thresh = self.buy_thresh
        elif mode == 'sell':
            thresh = self.sell_thresh
        else:
            raise ValueError(f"`mode` must be 'buy' or 'sell'; '{mode}' given.")
        # required relative strength to meet buy or sell threshold    
        req_rs = 100. / (100 - thresh) - 1
        req_growth = self.prov_rs.copy()
        # when zero daily change is required
        req_growth = req_growth.where(self.prov_rs!=req_rs, 0.)
        # when a positive daily change is required
        req_growth = req_growth.where(
            self.prov_rs>req_rs, self.downs*req_rs-self.ups
        )
        # when a negative daily change is required
        req_growth = req_growth.where(
            self.prov_rs<req_rs, self.downs-self.ups/req_rs
        )
        # calculate the target price from required daily growth
        target = (
            self.data.xs('close', axis=1, level=1).iloc[-1] * 
            (1 + req_growth / 100)
        )
        target = np.round(
            target.to_frame(name=f'Target'), 2
        )
        return target  
        
    def initial_action(self):
        """
        Assigns a provisional 'Wait to buy' or 'Wait to sell' action to each 
        stock depending on whether the RSI most recently exceeded 
        `self.sell_threshold` or `self.buy_threshold` respectively. Actual buy 
        or sell situations are subsequently identified by the 
        method `self.get_screener_data`, and where appropriate, these 
        provisional actions are replaced with 'BUY' or 'SELL'.
        
        Returns:
            action (pandas.DataFrame): contains the provisional action 'Wait to 
                buy' or 'Wait to sell' for each stock in `self.data`, based on 
                the current price relative to the buy and sell thresholds.
                   
        """
        # take a slice of `self.data` of correct shape
        action = self.data.xs('close', axis=1, level=1).iloc[-1].copy()
        # as a first guess, set actions for all stocks to 'Wait to buy'
        action[:] = 'Wait to buy'
        action = action.to_frame(name='Action')
        # determine whether wait to buy (most recently sold), or wait to sell
        # (most recently bought)
        rsi_data = stats.rsi(self.data, self.window, label='rsi')
        rsi_data = rsi_data.xs('rsi', axis=1, level=1)
        below_buy = rsi_data.where(rsi_data < self.buy_thresh)
        above_sell = rsi_data.where(rsi_data > self.sell_thresh)
        # find most recent day below or above buy or sell threshold
        for code in action.index:
            try:
                # get most recent dates when below buy and above sell thresholds
                last_below_buy = below_buy[code].dropna().index[-1]
                last_above_sell = above_sell[code].dropna().index[-1]
                # if last day below buy threshold is more recent than last day
                # above sell threshold, then in wait to sell scenario, and vice 
                # versa
                if last_below_buy > last_above_sell:
                    action.loc[code] = 'Wait to sell'
                # opposite to above applies
                else:
                    action.loc[code] = 'Wait to buy'
            # date query didn't return a result, so data must be too short. In
            # this case, preserve 'Wait' action until direction is established
            except:
                pass
        return action   

    def get_screener_data(self):
        """
        Replaces any provisional actions assigned by `self.initial_action` with
        'BUY' or 'SELL' actions, where such situations are identified. Returns
        a summary dataframe containing current price, target price, and
        corresponding action, for each stock in `self.data`.
        
        Returns:
            result (pandas.DataFrame): contains the current price (i.e. most
                recent close price), the target price, and the current action, 
                for each stock within `self.data`.
                
        """
        # get basic action to start with
        action = self.initial_action()
        # calculate target prices for buy and sell actions
        buy_target = self.calculate_target(mode='buy')
        sell_target = self.calculate_target(mode='sell')
        target = (
            buy_target.where(action['Action'] == 'Wait to buy', 0.) + 
            sell_target.where(action['Action'] == 'Wait to sell', 0.)
        )
        current = self.current_offline_data()
        # determine 'BUY' and 'SELL' actions - corresponds to the first
        # day below or above buy and sell targets respectively
        action[
            (current['Current']<target['Target']) & 
            (action['Action']=='Wait to buy')
        ] = 'BUY'
        action[
            (current['Current']>target['Target']) &
            (action['Action']=='Wait to sell')
        ] = 'SELL'
        # put it all together
        result = pd.concat([current, target, action], axis=1, sort=True)
        result.index.name = 'code'
        result.dropna(inplace=True)
        return result
    