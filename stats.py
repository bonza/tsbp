# -*- coding: utf-8 -*-
"""
Functions to calculate stock price statistics used in trading strategies.
Currently offers:
    
    - Consecutive days: the number of consecutive days above or below a price
      (or other value) threshold.
    - Relative change: the change in price (in percent) over a given multi-day
      interval.
    - Relative Strength Index (RSI): classic index which measures the size of 
      recent price changes to evaluate overbought or oversold conditions.
    - Simple Moving Average (SMA): the rolling average price, calculate over a
      window of fixed size.
      
"""
import operator
import numpy as np
import pandas as pd

# Default column labels for available statistics. Formatting of these occur 
# within the `_rebuild_result` function.
DEFAULT_LABELS = {
    'sma': 'sma_{}_{}',
    'relative_change': 'rc_{}_to_{}',
    'rsi': 'rsi_{}',
    'consecutive_days': 'consec_days_{}_{}_{}',
}

# Mapping of boolean operator strings to their respective functions.
MODE_FUNC_DICT = {
    '==': operator.eq,
    '>': operator.gt,
    '>=': operator.ge,
    '<': operator.lt,
    '<=': operator.le
}


def _rebuild_result(result, data, inplace, label, stat, *args):
    """
    This is a boilerplate function used by the statistic functions within this
    module. It appends the newly calculated statistic data to the original
    dataframe, preserving the original dataframe formatting.
    
    Args:
        result (pandas.DataFrame): pre-formatted dataframe containing the
            statistic data (a single column dataframe).
        data (pandas.DataFrame): the original dataframe to which the statistic
            data is to be appended.
        inplace (bool): whether all data from `data` is returned, or just the
            the statistic data (single column).
        label (str): the column name for the statistic data.
        stat (str): the name of the statistic (only used if `label` is NoneType, 
             in which case `stat` is a key from the `DEFAULT_LABELS` 
             dictionary).
            
    Returns
        result (pandas.DataFrame): formatted dataframe containing `data` plus 
            a new statistic column if `inplace` is True, otherwise only the
            statistic column.
            
    """
    # if `label` isn't provided, look up default label and format according
    # to the specific statistic parameters.
    if not label:
        label = DEFAULT_LABELS[stat]
        label = label.format(*args)
    # turn `result` dataframe's columns into a multiindex, and concatenate to
    # if specified.
    cols = [(company, label) for company in result.columns]
    result.columns = pd.MultiIndex.from_tuples(cols, names=['code', 'field'])        
    if inplace:
        result = pd.concat([data, result], axis=1)
        return result
    else:
        return result
    
    
def _average_gain_or_loss(data, window, mode):
    """
    Used in the `rsi` function. Calculates a Simple Moving Average of only
    either gain or loss days, where opposing days are capped at zero prior to 
    the SMA calculation.
    
    Args:
        data (pandas.DataFrame): data over which the average gain or loss is
            calculated.
        window (int): rolling window (days) over which average gain or loss is 
            calculated.
        mode (str): determines whether average gain ('>') or average loss ('<') 
            is calculated. 
    
    Returns:
        result (pandas.DataFrame): a single column dataframe containing the 
            average gain or loss, with the column name 'gain' or 'loss'
            respectively.
            
    """
    # calculate closing price change in percent previous close
    change = relative_change(data, 'close', -1, 'close', 0, label='change')
    change = change.xs('change', axis=1, level=1)
    # preserve days of only positive or negative growth, ensure absolute values
    disc = np.abs(change.where(MODE_FUNC_DICT[mode](change, 0.), 0.))
    # calculate SMA, and rebuild the result into usable dataframe
    result = disc.rolling(window, min_periods=window-1).mean()
    # give data custom label based on `mode` value
    result = _rebuild_result(
        result, data, False, 'gain' if mode == '>' else 'loss', None, window
    )
    return result


def consecutive_days(data, field, thresh, mode='>', inplace=False, label=None):
    """
    Determines the number of consecutive days prior to and including the 
    current day which meet a specified criterion.
    
    Args:
        data (pandas.DataFrame): contains data over which consecutive days are 
            calculated.  
        field (str): field in `data` over which consecutive days are calculated.
        thresh (float): threshold value in `field` for which consecutive days 
            are calculated.
        mode (str, default '>'): inequality determining how `threshold` value 
            is applied to data. Can be one of '==', '>', '>=', '<', '<='.
        inplace (bool, default False): whether consecutive days data is joined
            to the original `data` dataframe, or returned as a standalone 
            dataframe.
        label (str, default None): the name for the relative change data. If 
            not specified (default), the default consecutive days label is 
            looked up in the `DEFAULT_LABELS` dictionary.
            
    Returns:
        result (pandas.DataFrame): contains the calculated consecutive days 
        data.
        
    """
    def _consecutive_days_1d(disc):
        """
        Takes a single-column dataframe with each row containing a 1 or a 0, 
        and determines, for each row, the number of consecutive immediately 
        previous rows there are containing a 1 (and including the current row).
        
        """
        result = disc.copy()
        # iterate through each row and the row immediately previous to it
        for i in range(1, len(result)):
            current = result.iloc[i]
            previous = result.iloc[i-1]
            # if previous row is greater than zero, then add 1 and assign this
            # value to the current row
            result.iloc[i] = current * (previous + current)
        return result
       
    disc = data.xs(field, axis=1, level=1)
    # discretise the data according to whether each day meets the criterion or
    # not
    disc = disc.where(MODE_FUNC_DICT[mode](disc, thresh), 0)
    # for days that do, convert their value to a one (integer), to be fed into
    # `_consecutive_days_1d` function
    disc = disc.where(disc == 0, 1)
    disc = disc.astype(int)
    # apply `_consecutive_days_1d`
    result = disc.apply(_consecutive_days_1d)
    result = _rebuild_result(
        result, data, inplace, label, 'consecutive_days', field, mode, thresh
    )
    return result


def relative_change(
        data, from_field, from_day, to_field, to_day, inplace=False, label=None
    ):
    """
    Relative change in percent over a specified row-relative multi-day interval
    within `data`. 
    
    Args:
        data (pandas.DataFrame): contains data over which relative change is
            calculated. Handles data containing multiple stocks.
        from_field (str): field in `data` used for the starting point of the 
            relative change calculation.
        from_day (int): number of rows' distance relative to the current row 
            used for the starting point of the relative change calculation.
        to_field (str): field in `data` used for the end point of the relative 
            change calculation.
        to_day (int): number of rows' distance relative to the current row used 
            for the end point of the relative change calculation.
        inplace (bool, default False): whether the calculated SMA is joined
            to the original `data` dataframe, or returned as a standalone 
            dataframe.
        label (str, default None): the name for the relative change data. If 
            not specified (default), the default relative change label is 
            looked up in the `DEFAULT_LABELS` dictionary.
            
    Returns:
        result (pandas.DataFrame): contains the calculated relative change data.
            
    """
    # cross section `data` along `from_field` label, and shift according to 
    # `from_day` value
    start = data.xs(from_field, axis=1, level=1).shift(-1 * from_day)
    # cross section `data` along `to_field` label, and shift according to 
    # `to_day` value
    end = data.xs(to_field, axis=1, level=1).shift(-1 * to_day)
    # calculate change in percent between these two datasets
    result = 100 * (end - start) / start
    result = np.round(result, 3)
    result = _rebuild_result(
        result, data, inplace, label, 'relative_change', from_day, to_day
    )
    return result


def rsi(data, window, inplace=False, label=None):
    """
    Relative Strength Index, calculated using an n-day window of size `window`,
    based on the closing price.
    
    Args:
        data (pandas.DataFrame): contains a 'close' field over which RSI is
            calculated.
        window (int): size of the rolling window in days, used to calculate
            average gains and losses as necessary for the RSI calculation.
        inplace (bool, default False): whether the calculated RSI is joined
            to the original `data` dataframe, or returned as a standalone 
            dataframe.
        label (str, default None): the name for the RSI data. If not specified
            (default), then the default RSI label is looked up in the 
            `DEFAULT_LABELS` dictionary.
            
    Returns:
        result (pandas.DataFrame): contains the calculated RSI data.
        
    """
    # take rolling averages of gains and losses
    gain = _average_gain_or_loss(data, window, mode='>')
    gain = gain.xs('gain', axis=1, level=1)
    loss = _average_gain_or_loss(data, window, mode='<')
    loss = loss.xs('loss', axis=1, level=1)
    # calculate relative strength index
    result = np.round(100 - 100 / (1 + (gain / loss)), 1)
    result = _rebuild_result(result, data, inplace, label, 'rsi', window)
    return result


def sma(data, field, window, inplace=False, label=None):
    """
    Simple Moving Average, calculated over the column `field` using an n-day
    window of size `window`.
    
    Args:
        data (pandas.DataFrame): contains data over which the SMA is 
            calculated. Must contain a column whose label is given by `field` 
            (the target for the SMA calculation). Handles data containing 
            multiple stocks.
        field (str): field (column) over which the SMA is calculated.
        window (int): size of the rolling SMA window in days.
        inplace (bool, default False): whether the calculated SMA is joined
            to the original `data` dataframe, or returned as a standalone 
            dataframe.
        label (str, default None): the name for the SMA data. If not specified
            (default), then the default SMA label is looked up in the 
            `DEFAULT_LABELS` dictionary.
            
    Returns:
        result (pandas.DataFrame): contains the calculated SMA data.
            
    """
    # cross section `data` along `field` label (e.g. 'close').
    result = data.xs(field, axis=1, level=1)
    # take a rolling mean for each company
    result = result.rolling(window, min_periods=window-1).mean()
    result = np.round(result, 3)
    result = _rebuild_result(result, data, inplace, label, 'sma', field, window)
    return result
